create database recipes;
use recipes;

create table role(
	id int primary key auto_increment,
    role_type varchar(20) not null default 'user'
);

create table user(
	id int primary key auto_increment,
    username varchar(50) not null,
    created_at timestamp default now(),
    updated_at timestamp default now(),
    is_block boolean,
    foreign key(id) references role(id) 
);

create table recipe(
	id int primary key auto_increment,
    recipe_name varchar(50) not null,
    author int not null,
    -- ingredient varchar(200) not null,
    recipe_desc varchar(500) not null,
	created_at timestamp default now(),
    updated_at timestamp default now(),
    person int not null,
    is_show boolean,
    foreign key(author) references user(id)
    -- foreign key(ingredient) references ingredients(id)
);

create table ingredients(
	id int primary key auto_increment,
    ingre_name varchar(50),
    stock int
);

create table recipeingredients(
	recipe_id int not null,
    ingredients_id int not null,
    amount int not null,
    foreign key(recipe_id) references recipe(id),
    foreign key(ingredients_id) references ingredients(id)
);

create table comment(
	id int primary key auto_increment,
	recipe_id int not null,
    user_id int not null,
    comment_text varchar(200) not null,
    created_at timestamp default now(),
    updated_at timestamp default now(),
    foreign key(user_id) references user(id),
    foreign key(recipe_id) references recipe(id)
);

create table reply(
	id int primary key auto_increment,
    user_id int not null,
    comment_id int not null,
    reply_text varchar(200) not null,
    created_at timestamp default now(),
    updated_at timestamp default now(),
    foreign key(user_id) references user(id),
    foreign key(comment_id) references comment(id)
);

create table likes(
	recipe_id int not null,
    user_id int not null,
    created_at timestamp default now(),
    updated_at timestamp default now(),
    foreign key(user_id) references user(id),
	foreign key(recipe_id) references recipe(id),
    primary key(user_id,recipe_id)
);

insert into user(username,is_block)
values ('guddi',false),
	   ('sessa',false);
select * from user;

insert into recipe(recipe_name,author,recipe_desc,person,is_show)
values ('maggi',1,'take a empty vessel and put 500ml water and then put maggie into this and boil it from 10 minutes',1,true),
		('rice',2,'take a empty vessel and put 500ml water into vessel and put rice into it and boil it from 10 minutes',1,true);
select * from recipe;

insert into ingredients(id,ingre_name,stock)
values (1,'water',500),
	(2,'maggie packet',1),
       (3,'rice',1);
insert into ingredients(ingre_name,stock)  
values('sdfg',2);    
select * from ingredients;

insert into recipeingredients(recipe_id, ingredients_id,amount)
values (1,1,2),(2,2,2);

select ingre_name from ingredients;
select recipe_id,ingredients_id from recipeingredients;

select user.username,recipe_name,ingredients.ingre_name from recipe
join ingredients on ingredients.id=recipe.id 
join user on recipe.author = user.id
where user.id=2;


